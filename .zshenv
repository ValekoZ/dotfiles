export ZSH="$HOME/.oh-my-zsh"

export OPT="$HOME/.local/opt"

export PATH="$PATH:$HOME/.foundry/bin"
export PATH="$PATH:$HOME/.local/share/gem/ruby/3.0.0/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$OPT/depot_tools"
export PATH="$PATH:/opt/android-sdk/emulator"

export VISUAL="/usr/bin/nvim"
export EDITOR="$VISUAL"

export GTK_THEME="Catppuccin-Mocha-Standard-Red-dark:dark"

