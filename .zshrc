export GPG_TTY=$(tty)

HYPHEN_INSENSITIVE="true"
DISABLE_AUTO_TITLE="true"
# ENABLE_CORRECTION="true"
DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(
    starship
    git
    dotenv
    z
    tmuxinator
    zsh-autosuggestions
    zsh-interactive-cd
)

source $ZSH/oh-my-zsh.sh


# ZSH Auto Suggestions
bindkey '^ ' autosuggest-accept

export BAT_THEME="Catppuccin-mocha"

